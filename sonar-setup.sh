#!/bin/bash

# Install Java Development Kit (JDK) 11
sudo apt update
sudo apt install default-jdk -y

# Install and configure PostgreSQL
sudo apt install postgresql postgresql-contrib -y
sudo systemctl start postgresql
sudo systemctl enable postgresql
sudo -u postgres psql -c "CREATE USER sonarqube WITH ENCRYPTED PASSWORD 'your_password';"
sudo -u postgres psql -c "CREATE DATABASE sonarqube OWNER sonarqube;"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE sonarqube TO sonarqube;"

# Download and install SonarQube
wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.1.0.47736.zip
sudo unzip sonarqube-9.1.0.47736.zip -d /opt/
sudo mv /opt/sonarqube-9.1.0.47736 /opt/sonarqube

# Configure SonarQube to use PostgreSQL
sudo tee /opt/sonarqube/conf/sonar.properties > /dev/null <<EOF
sonar.jdbc.username=sonarqube
sonar.jdbc.password=your_password
sonar.jdbc.url=jdbc:postgresql://localhost/sonarqube
EOF

# Create a new system user for SonarQube
sudo useradd -r sonarqube -d /opt/sonarqube/

# Set permissions for the SonarQube directory
sudo chown -R sonarqube:sonarqube /opt/sonarqube/

# Configure SonarQube to start automatically at boot
sudo tee /etc/systemd/system/sonarqube.service > /dev/null <<EOF
[Unit]
Description=SonarQube service
After=syslog.target network.target postgresql.service

[Service]
Type=forking

ExecStart=/opt/sonarqube/bin/linux-x86-64/sonar.sh start
ExecStop=/opt/sonarqube/bin/linux-x86-64/sonar.sh stop

User=sonarqube
Group=sonarqube
Restart=always

LimitNOFILE=65536
LimitNPROC=4096

[Install]
WantedBy=multi-user.target
EOF

# Reload the systemd daemon and start SonarQube
sudo systemctl daemon-reload
sudo systemctl start sonarqube
sudo systemctl enable sonarqube

# Configure firewall rules to allow access to SonarQube
sudo ufw allow 9000/tcp

# Output the URL to access SonarQube
echo "SonarQube is now accessible at http://localhost:9000"
