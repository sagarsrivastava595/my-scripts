#!/bin/bash

# Define the target IP address and port
target_ip="127.0.0.1" # Change this to the IP address where you want to check port 8080
port=8080

# Try to connect to the port using netcat (nc)
nc -zv "$target_ip" "$port"

# Check the exit code of the previous command
if [ $? -eq 0 ]; then
  echo "Port $port is working and accessible on $target_ip."
else
  echo "Port $port is NOT working or accessible on $target_ip."
fi
