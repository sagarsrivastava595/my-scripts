#!/bin/bash

# Check the current swap size
echo "Current swap size:"
sudo swapon --show

# Disable the existing swap
sudo swapoff -a

# Create a new swap file with 4GB size
sudo fallocate -l 4G /swapfile

# Set the correct permissions for the swap file
sudo chmod 600 /swapfile

# Format the file as swap space
sudo mkswap /swapfile

# Enable the new swap file
sudo swapon /swapfile

# Update the fstab file to make the changes persistent after reboot
echo "/swapfile none swap sw 0 0" | sudo tee -a /etc/fstab

# Verify the new swap size
echo "New swap size:"
sudo swapon --show

# Display the updated sysctl settings
echo "Sysctl settings:"
sudo sysctl vm.swappiness vm.vfs_cache_pressure
