#!/bin/bash

# Replace these variables with your desired values
DB_USER="upsc_poc"
DB_PASSWORD="upsc_poc_ocr2024"
DB_NAME="upsc_poc_ocr"

## PostgreSQL administrative credentials
#PG_ADMIN_USER="postgres"
#PG_ADMIN_PASSWORD="postgres_password"

# Function to create PostgreSQL user
create_user() {
  sudo -u postgres psql -c "CREATE USER $DB_USER WITH PASSWORD '$DB_PASSWORD';"
}

# Function to create PostgreSQL database
create_database() {
  sudo -u postgres createdb $DB_NAME
  sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;"
}

# Main script
echo "Creating PostgreSQL user..."
create_user

echo "Creating PostgreSQL database..."
create_database

echo "User and database created successfully."
