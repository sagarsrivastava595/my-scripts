#!/bin/bash
dbname=abc
dbpassword=abc@123

# Update system packages
sudo apt-get update

# Install PostgreSQL
sudo apt-get install postgresql-12 -y

# Start PostgreSQL service
sudo service postgresql start

# Configure PostgreSQL to allow remote connections
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/12/main/postgresql.conf

# Allow remote connections from any IP address
echo "host    all             all             0.0.0.0/32          md5" | sudo tee -a /etc/postgresql/12/main/pg_hba.conf > /dev/null

# Restart PostgreSQL service for changes to take effect
sudo service postgresql restart

# Create a new PostgreSQL user (optional)
sudo -u postgres createuser --interactive

# Create a new PostgreSQL database (optional)
sudo -u postgres createdb $dbname

# Access the PostgreSQL interactive terminal (optional)
sudo -u postgres psql

# Done!
echo "PostgreSQL installation and configuration completed."

