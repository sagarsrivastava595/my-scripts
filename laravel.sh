#! /bin/bash
apt-get update
apt-get install -y apache2
apt-get install -y php
apt-get install -y mysql-server
apt-get install  -y curl
apt-get install -y composer
curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/bin --filename=composer
cd /tmp || exit
echo "Downloading Postman ..."
wget -q https://dl.pstmn.io/download/latest/linux?arch=64 -O postman.tar.gz
tar -xzf postman.tar.gz
rm postman.tar.gz

echo "Installing to opt..."
if [ -d "/opt/Postman" ];then
    sudo rm -rf /opt/Postman
fi
sudo mv Postman /opt/Postman

echo "Creating symbolic link..."
if [ -L "/usr/bin/postman" ];then
    sudo rm -f /usr/bin/postman
fi
sudo ln -s /opt/Postman/Postman /usr/bin/postman

echo "[Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=/opt/Postman/app/Postman 
Icon=/opt/Postman/app/resources/app/assets/icon.png
Terminal=false
Type=Application
Categories=Development;" >> /home/singsys/Desktop/postman.desktop

echo "Installation completed successfully."
echo "You can use Postman!"
 
apt-get install -y  php
apt-get install -y openjdk-11-jdk

apt-get install -y   php-bcmath php-cli php-common php-curl php-gd php-intl php-json php-mbstring php-mysql php-soap php-xml php-xmlrpc php-zip php-xsl

apt-get install -y php8.0  php8.0-bcmath php8.0-cli php8.0-common php8.0-curl php8.0-gd php8.0-intl php8.0-json php8.0-mbstring php8.0-mysql php8.0-soap php8.0-xml php8.0-xmlrpc php8.0-zip php8.0-xsl
  apt install ssh -y
  apt install snapd -y
add-apt-repository -y ppa:ondrej/php
apt-get -y update
apt-get install -y php8.0 php8.0-bcmath php8.0-cli php8.0-common php8.0-curl php8.0-gd php8.0-intl php8.0-json php8.0-mbstring php8.0-mysql php7.3-soap php7.3-xml php7.3-xmlrpc php7.3-zip php7.3-xsl


a2enmod rewrite
service apache2 restart
service mysql restart
chown -R ubuntu:ubuntu /var/www
echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
